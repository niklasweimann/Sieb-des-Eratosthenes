﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sieb_des_Eratosthenes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int data = 1;
            while(File.Exists(Path.Combine(Application.StartupPath, "Test "+data +".txt")))
            {
                data++;
            }

            string locpath = Path.Combine(Application.StartupPath, "Test " + data + ".txt");

            if (File.Exists(locpath))
            {
                File.Delete(locpath);
            }
            textBox1.Enabled = false;
            Int64 x = Convert.ToInt64(textBox1.Text);
            for (Int64 i = 0; i < x; i++)
            {
                if(Fast(i) == true)
                {
                    StreamWriter file1 = new StreamWriter(locpath, true);
                    file1.WriteLine(i);
                    file1.Close();
                    label2.Text = i.ToString();
                }
            }
            textBox1.Enabled = true;
        }

        public static bool Fast(long testNumber)
        {
            if (testNumber < 2) return false;

            if (testNumber % 2 == 0) return false;

            long upperBorder = (long)System.Math.Round(System.Math.Sqrt(testNumber), 0);
            for (long i = 3; i <= upperBorder; i = i + 2)
                if (testNumber % i == 0) return false;
            return true;
        }
    }
}
